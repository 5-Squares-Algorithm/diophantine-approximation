{-
    Helper functions for Points module.
-}

module Decomposition where
import Vector
import Auxiliary

{-
    Writes y as a linear combination of u1, u2, and their cross-product. Uses Cramer's rule.
-}
coefficient :: Vector [Integer] -> Vector[Integer] -> Vector [Double] -> [Double]
coefficient u1 u2 y = [a,b,c]
    where
        Vector [a1,b1,c1] = fromIntegralVector u1
        Vector [a2,b2,c2] = fromIntegralVector u2
        Vector [a3,b3,c3] = outer_product (Vector [a1,b1,c1]) (Vector [a2,b2,c2])
        a = (det y (Vector [a2,b2,c2]) (Vector [a3,b3,c3])) / (det (Vector [a1,b1,c1]) (Vector [a2,b2,c2]) (Vector [a3,b3,c3]))
        b = (det (Vector [a1,b1,c1]) y (Vector [a3,b3,c3])) / (det (Vector [a1,b1,c1]) (Vector [a2,b2,c2]) (Vector [a3,b3,c3]))
        c = (det (Vector [a1,b1,c1]) (Vector [a2,b2,c2]) y) / (det (Vector [a1,b1,c1]) (Vector [a2,b2,c2]) (Vector [a3,b3,c3]))

{-
    Returns the determinant of the matrix spanned by the given vectors.
-}
det :: Vector [Double] -> Vector [Double] -> Vector [Double] -> Double
det (Vector [a1,b1,c1]) (Vector [a2,b2,c2]) (Vector [a3,b3,c3]) = a1*b2*c3+a3*b1*c2+a2*b3*c1-a3*b2*c1-a1*b3*c2-a2*b1*c3

{-
    Cross-product of two vectors.
-}
outer_product :: Vector [Double] -> Vector [Double] -> Vector [Double]
outer_product (Vector [a1, a2, a3]) (Vector [b1, b2, b3]) = Vector [a2*b3-a3*b2, a3*b1-a1*b3, a1*b2-a2*b1]

{-
    Returns the length of the projection of y onto the orthogonal complement of u1 and u2.
-}
perp :: Vector [Integer] -> Vector [Integer] -> Vector [Double] -> Double
perp u1 u2 y = normm $ scalar_mul c $ outer_product (fromIntegralVector u1) (fromIntegralVector u2)
    where [a,b,c] = coefficient u1 u2 y
