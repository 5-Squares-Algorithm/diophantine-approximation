{-
    Vector data type and operations
-}

module Vector where
import Data.Maybe

-- type parameter should represent a list of Integral numbers
data Vector a = Vector a | Failure deriving (Show, Eq)
-- Vector is a Functor
instance Functor Vector where
    fmap f (Vector x) = Vector $ f x
    fmap f Failure = Failure

{-
    Vector addition
-}
v_sum :: Num t => Vector [t] -> Vector [t] -> Vector [t]
v_sum (Vector []) (Vector []) = Vector []
v_sum (Vector (x:xs)) (Vector (y:ys)) = fmap ((x+y):) $ v_sum (Vector xs) (Vector ys)
v_sum (Vector []) (Vector (x:xs)) = Failure
v_sum (Vector (x:xs)) (Vector []) = Failure
v_sum Failure _ = Failure
v_sum _ Failure = Failure

{-
    Vector subtraction
-}
v_diff :: Num t => Vector [t] -> Vector [t] -> Vector [t]
v_diff (Vector []) (Vector []) = Vector []
v_diff (Vector (x:xs)) (Vector (y:ys)) = fmap ((x-y):) $ v_diff (Vector xs) (Vector ys)
v_diff (Vector []) (Vector (x:xs)) = Failure
v_diff (Vector (x:xs)) (Vector []) = Failure
v_diff Failure _ = Failure
v_diff _ Failure = Failure

{-
    Multiplication of a vector by a scalar
-}
scalar_mul :: Num t => t -> Vector [t] -> Vector [t]  
scalar_mul _ Failure = Failure
scalar_mul c v = fmap (map (*c)) v

{-
    Dot product of vectors.
-}
dot_product :: Num t => Vector [t] -> Vector [t] -> Maybe t
dot_product (Vector []) (Vector []) = Just 0
dot_product (Vector (x:xs)) (Vector (y:ys)) = (dot_product (Vector xs) (Vector ys)) >>=
    (\p -> return $ p + x*y)
dot_product (Vector []) (Vector (x:xs)) = Nothing
dot_product (Vector (x:xs)) (Vector []) = Nothing
dot_product Failure _ = Nothing
dot_product _ Failure = Nothing
-- dot applies dot_product, then fromJust. Only use if confident Nothing is not returned
dot :: Num t => Vector [t] -> Vector [t] -> t
dot v1 v2 = fromJust $ dot_product v1 v2

{-
    Returns the distance between two points.
-}
dist2 :: Num t => Vector [t] -> Vector [t] -> t
dist2 v1 v2 = normm (v1 `v_diff` v2)

{-
    Returns the square norm of a vector.
-}
normm :: Num t => Vector [t] -> t 
normm v = v `dot` v

{-
    fromIntegral extended to vectors
-}
fromIntegralVector :: (Integral t, Num n) => Vector [t] -> Vector [n]
fromIntegralVector v = fmap (map fromIntegral) v

{-
    round extended to vectors
-}
roundVector :: (Integral t, RealFrac n) => Vector [n] -> Vector [t]
roundVector v = fmap (map round) v