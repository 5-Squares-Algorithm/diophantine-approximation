This code is used for the numerical verification of the arithmetic conjecture 1.1 for d = 4 in the following paper:
https://arxiv.org/abs/1811.06831

To find an integral lift in Z^5 for a^2 + b^2 + c^2 = N, run solve in 'Solve.hs'.
To find an integral lift of a point in S^2(Z/qZ) as a subset of S^4(Z/qZ), use lift in 'Lift.hs'.

The factoring code and many ideas for the rest of the code are from the third author's code in [Sar17b].
We thank Peter Selinger for publically prividing the Quantum.Synthesis modules in the newsynth package.
We also thank Brandon Boggess for helping develop this code.