{-
    This file contains various helper functions used in Solve and Asymptotic. Most of these are
    imported directly from the four-squares algorithm of Naser T. Sardari.
-}

module Auxiliary where
import Quantum.Synthesis.EuclideanDomain
import Quantum.Synthesis.Diophantine
import Quantum.Synthesis.StepComp
import Quantum.Synthesis.Ring
import Data.Maybe
import Control.Exception
import System.Random

{------------------------------------------------------------------------
Sum of 2 Squares Functions
------------------------------------------------------------------------}

{-
    Given a prime input n, returns a Gaussian integer of norm n, if one exists, or Nothing otherwise.
    If non-prime number is passed in, this function diverges.
-}
prime_to_ZComplex :: (RandomGen g) => g -> Integer -> StepComp (Maybe ZComplex)
prime_to_ZComplex g n
  | n < 0 = prime_to_ZComplex g (-n)
  | n == 0 = return (Just 0)
  | n == 2 = return (Just (1+i))
  | n_mod_4 == 1 = do
    h <- root_of_negative_one g n
    let t = euclid_gcd (fromInteger h+i) (fromInteger n) :: ZComplex
    assert (adj t * t == fromInteger n) $ return (Just t)
  | n_mod_4 == 3 = 
    return Nothing
  where
    n_mod_4 = n `mod` 4

{-
    Given a factorization of n into distinct prime powers,
    returns t ∈ ℤ[ω] with norm n or Nothing if none exists.
-}
factorization_to_ZComplex :: (RandomGen g) => g -> [(Integer, Integer)] -> StepComp (Maybe ZComplex)
factorization_to_ZComplex g facs = do
  res <- parallel_list_maybe [power_to_ZComplex g (n,k) | (n,k) <- facs]
  case res of
    Nothing -> return Nothing
    Just sols -> return (Just (product sols))

{-
    Given a pair of integers n and k, returns t ∈ ℤ[ω] with norm n^k or Nothing if none exists.
-}
power_to_ZComplex :: (RandomGen g) => g -> (Integer, Integer) -> StepComp (Maybe ZComplex)
power_to_ZComplex g (n,k)
  | even k = return (Just (fromInteger (n^(k `div` 2))))
  | otherwise = do
    t <- find_ZComplex g n
    case t of
      Nothing -> return Nothing
      Just t -> return (Just (t^k))

{-
    Given input n, returns a Gaussian integer of norm n or Nothing if none exists.
-}
find_ZComplex :: (RandomGen g) => g -> Integer -> StepComp (Maybe ZComplex)
find_ZComplex g n
  | n < 0 = find_ZComplex g (-n)
  | n == 0 = return (Just 0)
  | n == 1 = return (Just 1)
  | euclid_mod n 4 == 0 = fmap (fmap (2*)) (find_ZComplex g (euclid_div n 4))
  | euclid_mod n 2 == 0 = fmap (fmap ((1 +i) *)) (find_ZComplex g (euclid_div n 2))
  | otherwise = interleave prime_solver factor_solver where
    interleave p f = do
      p <- subtask 4 p
      case p of 
        Done res -> return res
        _ -> do
          f <- subtask 1000 f
          case f of
            Done (a, k) -> do
              let b = n `div` a
              let (u, facs) = relatively_prime_factors a b
              forward (k `div` 2) $ factorization_to_ZComplex g3 facs
            _ -> interleave p f
  
    (g1, g') = split g
    (g2, g3) = split g'
    prime_solver = prime_to_ZComplex g1 n 
    factor_solver = with_counter $ speedup 30 $ find_factor g2 n

{------------------------------------------------------------------------
Auxiliary Functions
------------------------------------------------------------------------}

{-
    Returns the Jacobi symbol: 1 if n is a square mod q, -1 otherwise.
-}
qresidue :: Integer -> Integer -> Integer
qresidue n q 
  | n==1 = 1
  |((abs n) >q) = qresidue (euclid_mod n q) q
  |((euclid_mod n 4) == 0)   = qresidue (euclid_div n 4) q
  |((euclid_mod n 2) == 0)   = (qresidue (euclid_div n 2) q)* (m q)
  |otherwise = qresidue (q* (l n q)) n

{-
    Jacobi symbol mod 8
-}
m :: (Num a, EuclideanDomain a1) => a1 -> a
m q
 | (euclid_mod q 8 ==1 || euclid_mod q 8 ==7 ) = 1
 | (euclid_mod q 8 ==3 || euclid_mod q 8 ==5 ) = (-1)        
 
{-
    Jacobi symbol of 2 mod q.
-} 
l :: Integer -> Integer -> Integer
l n q
 | ((euclid_mod (qq*nn) 2) ==1) = (-1) 
 | ((euclid_mod (qq*nn) 2) ==0) = 1
 where
  qq=euclid_div (q-1) 2
  nn=euclid_div (n-1) 2 

{-
    Given prime q and integer a, returns a^-1 mod q
-}
inv' :: EuclideanDomain a => a -> a -> a
inv' q a = fromJust $ inv_mod q a


{-
    Given a and q, reduces a to a minimal residue mod q
-}
minimal :: Integer -> Integer -> Integer
minimal a q
  |abs(2*a) < q = a
  |otherwise = if (2*m < q)
    then m
    else m - q
  where m = euclid_mod a q

{-
    Checks if a given input a is a minimal residue mod q
-}
check_minimal :: Integer -> Integer -> Bool
check_minimal a q = abs(2*a) < q

{-
      Repeats each element of an array a specified number (n) of times.
-}
repeat_list :: Int -> [a] -> [a]
repeat_list n (x:xs) = (take n $ repeat x) ++ repeat_list n xs
repeat_list _ [] = []