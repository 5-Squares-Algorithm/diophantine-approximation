{-
    This program lifts a local solution of the form (a,b,c,0,0) mod q to a global solution (x1,x2,x3,x4,x5) in poly(log) time.
    Global solution satisfies x1^2 + x2^2 + x3^2 + x4^2 + x5^2 = n for some n.

    Inputs:
    Local solution (a,b,c,0,0) mod q, q prime
    Any integer n
    Random number for factorization algorithm

    Outputs:
    Global integral solution (x1,x2,x3,x4,x5)
-}

module Solve where
import Auxiliary
import LLL
import Vector
import Points
import Quantum.Synthesis.EuclideanDomain
import Quantum.Synthesis.StepComp
import Quantum.Synthesis.Ring
import Data.Maybe
import Control.Exception
import System.Random
import Decomposition

{------------------------------------------------------------------------
Main Application
------------------------------------------------------------------------}

{-
    Solves the problem of representing n as a sum of 5 squares subject to congruence conditions.
-}
solve :: (RandomGen t1) => t1 -> Integer -> Integer -> Integer -> Integer -> Integer -> Int -> Maybe (Vector [Integer])
solve g a b c n q effort
    |not(check_minimal a q && check_minimal b q && check_minimal c q)
        = solve g (minimal a q) (minimal b q) (minimal c q) n q effort
    |otherwise = first_solvable g candidates where
    candidates = listpoint a b c n q
    first_solvable g [] = Nothing
    first_solvable g (Vector [t1,t2,t3] : ts) = case answer_t of
        Just (Just (Cplx t4 t5)) -> Just (Vector [q*t1+a,q*t2+b,q*t3+c,q*t4,q*t5])
        _ -> first_solvable g2 ts
        where
            (g1, g2) = split g
            ti = fromIntegral $ div (n - (q*t1+a)^2 - (q*t2+b)^2 - (q*t3+c)^2) (q^2)
            answer_t = if ti < 0 then Nothing else run_bounded effort $ find_ZComplex g1 ti

{------------------------------------------------------------------------
Lattice Functions
------------------------------------------------------------------------}

{-
    Given a, b, c, and prime q, returns a basis spanning the lattice ax + by + cz = 0 mod q
-}
lattice :: Integer -> Integer -> Integer -> Integer -> [Vector [Integer]]
lattice a b c q
    |euclid_mod a q /= 0 = [Vector [q,0,0], Vector [ba,1,0], Vector [ca,0,1]]
    |euclid_mod b q /= 0 = [Vector [1,0,0], Vector [0,q,0], Vector [0,cb,1]]
    |otherwise           = [Vector [1,0,0], Vector [0,1,0], Vector [0,0,q]]
    where
        ba = euclid_mod ((-1)* b * (inv' q a)) q
        ca = euclid_mod ((-1)* c * (inv' q a)) q
        cb = euclid_mod ((-1)* c * (inv' q b)) q


{-
    Reduces the first vector to the fundemental domain of the other three
-}
euclidbasis :: Vector [Integer] -> Vector [Integer] -> Vector [Integer] -> Vector [Integer] -> Integer -> Vector [Integer]
euclidbasis (Vector [x,y,z]) (Vector [a1,b1,c1]) (Vector [a2,b2,c2]) (Vector [a3,b3,c3]) q = Vector [r,s,t]
    where
        r = minimal (euclid_mod z1 (q^2)) (q^2)
        s = minimal (euclid_mod z2 (q^2)) (q^2)
        t = minimal (euclid_mod z3 (q^2)) (q^2)
        z1 = euclid_div (x*(b2*c3 - b3*c2) - a2*(y*c3 - b3*z) + a3*(y*c2 - b2*z)) det
        z2 = euclid_div (a1*(y*c3 - b3*z) - x*(b1*c3 - b3*c1) + a3*(b1*z - y*c1)) det
        z3 = euclid_div (a1*(b2*z - y*c2) - a2*(b1*z - y*c1) + x*(b1*c2 - b2*c1)) det
        det = a1*(b2*c3 - b3*c2) - a2*(b1*c3 - b3*c1) + a3*(b1*c2 - b2*c1)

{-
    This function finds a solution mod q for 2(t1*a + t2*b + t3*c) = (n- a^2 - b^2 - c^2)/q mod q
-}
modsol :: Integer -> Integer -> Integer -> Integer -> Integer -> Vector [Integer]
modsol a b c n q 
    | euclid_mod g q == 0 = Vector [0,0,0]
    | euclid_mod a q /= 0 = Vector [r,0,0]
    | euclid_mod b q /= 0 = Vector [0,s,0]
    | otherwise           = Vector [0,0,t]
    where 
        r = euclid_mod ((inv' q (2*a))*g) q
        s = euclid_mod ((inv' q (2*b))*g) q
        t = euclid_mod ((inv' q (2*c))*g) q
        g = euclid_div (n - a*a - b*b - c*c) q

{-
    Checks that the given (a,b,c) triple is consistent with n and q as a local solution
-}
checkq :: Integer -> Integer -> Integer -> Integer -> Integer -> Bool
checkq a b c n q = (r/=0)
    where 
        r = euclid_mod (n - a^2 - b^2 - c^2) q

{-
    Returns a list of at most 1000 candidates that satisfy the congruence condition imposed by the shifted
    lattice 2*a*t1 + 2*b*t2 + 2*c*t3 = (n - a^2 - b^2 - c^2)/q mod q
-}
listpoint :: Integer -> Integer -> Integer -> Integer -> Integer -> [Vector [Integer]]
listpoint a b c n q
    |checkq a b c n q = [] -- if local conditions fail, no candidates

    -- check if the dimension of the problem should be reduced
    |abs (fromIntegral r3) > t3 * fromIntegral q^2 = recurse u1 u2 u3 (take 10 $ interleaved n)
    |abs (fromIntegral r2) > t2 * fromIntegral q^2 = recurse u1 u3 u2 (take 10 $ interleaved n)
    |abs (fromIntegral r1) > t1 * fromIntegral q^2 = recurse u2 u3 u1 (take 10 $ interleaved n)

    -- generic case
    |otherwise  = [u| Vector [k1,k2,k3] <- take 1000 $ points n1 n2 n3,
        let u = u0 `v_sum` (scalar_mul k1 u1) `v_sum` (scalar_mul k2 u2) `v_sum` (scalar_mul k3 u3)]
        where
            [u1, u2, u3] = reduce (lattice a b c q) 0.75 -- LLL-reduced basis

            -- particular solution
            Vector [r1, r2, r3] = euclidbasis ((Vector [a*q,b*q,c*q]) `v_sum` (scalar_mul (q^2) $ modsol a b c n q)) u1 u2 u3 q
            u0 = fmap (map $ \x -> euclid_div x $ q^2) $ (scalar_mul r1 u1) `v_sum` (scalar_mul r2 u2) `v_sum` (scalar_mul r3 u3) `v_diff` (Vector [a*q, b*q, c*q])

            -- bounds of the box
            t1 = (27/sqrt 8)*(sqrt (fromIntegral n) / fromIntegral q)/(sqrt $ fromIntegral $ normm u1)
            t2 = (27/sqrt 8)*(sqrt (fromIntegral n) / fromIntegral q)/(sqrt $ fromIntegral $ normm u2)
            t3 = (27/sqrt 8)*(sqrt (fromIntegral n) / fromIntegral q)/(sqrt $ fromIntegral $ normm u3)

            -- bounds for points function
            n1 = floor $ t1 + (abs $ fromIntegral r1)/(fromIntegral q^2)
            n2 = floor $ t2 + (abs $ fromIntegral r2)/(fromIntegral q^2)
            n3 = floor $ t3 + (abs $ fromIntegral r3)/(fromIntegral q^2)

            -- recursion for dimension reduction
            recurse v1 v2 v3 (x:xs) = (listpoint2 u0 v1 v2 v3 x a b c n q) ++ (recurse v1 v2 v3 xs)
            recurse _ _ _ [] = []

{-
    Called when the box needed in listpoint is empty
-}
listpoint2 :: Vector [Integer] -> Vector [Integer] -> Vector [Integer] -> Vector [Integer] -> Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> [Vector [Integer]]
listpoint2 u0 u1 u2 u3 l a b c n q
    |v_length > n'/(q')^2 = []

    |abs r2 > t2 = recurse u1 u2 (take 10 $ interleaved $ floor $ t2 + abs r2)
    |abs r1 > t1 = recurse u2 u1 (take 10 $ interleaved $ floor $ t1 + abs r1)

    |otherwise = [u| Vector [k1,k2,_] <- take 100 $ points (floor t1) (floor t2) 0,
        let u = u0 `v_sum` (scalar_mul (k1 - h1) u1) `v_sum` (scalar_mul (k2 - h2) u2) `v_sum` (scalar_mul l u3)]
        where
            v_length = perp u1 u2 y
            y = Vector [a'/q',b'/q',c'/q'] `v_sum` (fromIntegralVector u0) `v_sum` (fromIntegralVector $ scalar_mul l u3)
            
            a' = fromIntegral a
            b' = fromIntegral b
            c' = fromIntegral c
            n' = fromIntegral n
            q' = fromIntegral q
            
            [d1,d2,_] = coefficient u1 u2 y
            [h1,h2] = map round [d1,d2]
            r1 = d1 - (fromIntegral h1)
            r2 = d2 - (fromIntegral h2)
            m = sqrt (n'/(q')^2 - v_length)
            t1 = m / ((sqrt 2) * (sqrt $ fromIntegral $ normm u1))
            t2 = m / ((sqrt 2) * (sqrt $ fromIntegral $ normm u2))

            recurse v1 v2 (x:xs) = (listpoint1 u0 v1 v2 u3 x l a b c n q) ++ (recurse v1 v2 xs)
            recurse _ _ [] = []

{-
    Returns a list of candidates in one dimensional case when the box is empty that satisfy the
    congruence condition imposed by the shifted lattice 2*a*t1 + 2*b*t2 + 2*c*t3 =
    (n - a^2 - b^2 - c^2)/q mod q
-}
listpoint1 :: Vector [Integer] -> Vector [Integer] -> Vector [Integer] -> Vector [Integer] -> Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> [Vector [Integer]]
listpoint1 u0 u1 u2 u3 l2 l3 a b c n q
    |disc < 0 = []
    |otherwise =  [u| l1 <- take 10 $ map (div (k2 + k1) 2 + ) $ interleaved $ div (k2 - k1) 2,
    let u = u0 `v_sum` (scalar_mul l1 u1) `v_sum` (scalar_mul l2 u2) `v_sum` (scalar_mul l3 u3)]
        where 
        disc = b1^2 - 4*a1*c1
        w = Vector [fromIntegral a / fromIntegral q, fromIntegral b / fromIntegral q, fromIntegral c / fromIntegral q]
        
        -- coefficients
        a1 = fromIntegral $ normm u1
        b1 = 2*(fromIntegralVector u1 `dot` (fromIntegralVector (u0 `v_sum` (scalar_mul l2 u2) `v_sum` (scalar_mul l3 u3)) `v_sum` w))
        c1 = normm (fromIntegralVector (u0 `v_sum` (scalar_mul l2 u2) `v_sum` (scalar_mul l3 u3)) `v_sum` w) - (fromIntegral n / (fromIntegral $ q^2))
        
        -- quadratic formula
        k1 = ceiling $ (-b1 - sqrt disc) / (2 * a1)
        k2 = floor $ (-b1 + sqrt disc) / (2 * a1)