{-
    This module contains performs the LLL lattice algorithm.
    Input: A basis [v1,v2,v3] for some lattice.
    Output: An almost-orthogonal basis [u1,u2,u3] for that lattice.
-}

module LLL where
import Quantum.Synthesis.EuclideanDomain
import Vector
import Data.Maybe

{------------------------------------------------------------------------
Main Function
------------------------------------------------------------------------}

{-
    For an input basis (v1,v2,v3) and delta, returns a delta-LLL reduced basis (u1,u2,u3).
-}
reduce :: Integral t => [Vector [t]] -> Double -> [Vector [t]]
reduce list delta = do
    let gram = orthoBasis list -- compute Gram-Schmidt basis
    let reduced = reduction list gram -- recursive reduction step
    if lovasz reduced gram delta -- recursively check Lovasz condition for all vactors
        then reduced -- if satisfied, return reduced
        else reduce (swap reduced gram delta) delta -- if not, return to start

{------------------------------------------------------------------------
Compute Gram-Schmidt
------------------------------------------------------------------------}

mu :: Integral t => Vector [t] -> Vector [Double] -> Double
mu v u = ((fromIntegralVector v) `dot` u) / (normm u)
    
{-
    Returns the Gram-Schmidt orthogonalization of a set of integral vectors. Hard-coded.
-}
orthoBasis :: Integral t => [Vector [t]] -> [Vector [Double]]
orthoBasis [v1,v2,v3] = [v1',v2',v3']
    where
        v1' = fromIntegralVector v1
        v2' = (fromIntegralVector v2) `v_diff` ((mu v2 v1') `scalar_mul` v1')
        v3' = (fromIntegralVector v3) `v_diff`
            ((mu v3 v1') `scalar_mul` v1') `v_diff` ((mu v3 v2') `scalar_mul` v2')

{------------------------------------------------------------------------
Reduction Step
------------------------------------------------------------------------}

{-
    Performs the reduction step of the algorithm. Hard-coded.
-}
reduction :: Integral t => [Vector [t]] -> [Vector [Double]] -> [Vector [t]]
reduction [v1,v2,v3] [u1,u2,u3] = [v1,v2',v3''] where
    v2' = v2 `v_diff` ((round $ mu v2 u1) `scalar_mul` v1)
    v3' = v3 `v_diff` ((round $ mu v3 u2) `scalar_mul` v2)
    v3'' = v3' `v_diff` ((round $ mu v3' u1) `scalar_mul` v1)

{------------------------------------------------------------------------
Swap Step
------------------------------------------------------------------------}

lovasz :: Integral t => [Vector [t]] -> [Vector [Double]] -> Double -> Bool
lovasz [v1,v2,v3] [u1,u2,u3] delta = 
    delta * normm u1 <= normm (u2 `v_sum` ((mu v2 u1) `scalar_mul` u1)) &&
    delta * normm u2 <= normm (u3 `v_sum` ((mu v3 u2) `scalar_mul` u2))

swap :: Integral t => [Vector [t]] -> [Vector [Double]] -> Double -> [Vector [t]]
swap [v1,v2,v3] [u1,u2,u3] delta
    |delta * normm u1 > normm (u2 `v_sum` ((mu v2 u1) `scalar_mul` u1)) = [v2,v1,v3]
    |otherwise = [v1,v3,v2]