{-
    This module uses the 'Solve' module to find the minimal lift of a point in S^2(Z/qZ) as a subset of S^4(Z/qZ).
-}

module Lift where
import Solve
import LLL
import Vector
import Auxiliary
import Quantum.Synthesis.EuclideanDomain
import Quantum.Synthesis.Diophantine
import Quantum.Synthesis.GridSynth
import Quantum.Synthesis.StepComp
import Quantum.Synthesis.Ring
import Data.List.Split (splitOn)
import Data.Number.FixedPrec
import Data.Function
import Data.Maybe
import System.Random

{-
    Finds the minimal lift of a point (a, b, c) in S^2(Z/qZ) as a subset of S^4(Z/qZ).
-}
lift :: (RandomGen t1) => t1 -> Integer -> Integer -> Integer -> Integer -> Integer -> Int -> Maybe (Vector [Integer], Integer)
lift g a b c p q effort
    |bool = search 0 2 -- only check even powers of p
    |otherwise = Nothing
    where
        bool = qresidue (a^2 + b^2 + c^2) q == 1
        search h inc
            |h > cuttoff = Nothing -- beyond some point, the search stops automatically; this should never happen
            |test == Nothing = search (h + inc) inc
            |otherwise = Just (fromJust test, h)
            where
                [a', b', c'] = proj g a b c (p^h) q effort -- ensure local conditions are satisfied
                test = solve g a' b' c' (p^h) q effort -- obtain solution for particular power of p
                cuttoff = floor $ 5 * (log $ fromIntegral q)/(log $ fromIntegral p)

{------------------------------------------------------------------------
Helper Functions
------------------------------------------------------------------------}

{-
    A list of values of q to use for numerical tests. Repeats each prime 100 times.
-}
primes :: [Integer]
primes = repeat_list 100 [
    8714772976381697859557965355791745997987728300226812732519702250463329265826033268926171610392211750044957719896320776794555146581,
    4486847188052919998840445633197185837412694555920470505272960880292118699574860419602025075639197639973623241174554640239940468491,
    3007481519774841700447169103699480479257075158004429172505493169624211738165626204221653115518171895124254955050192476647393240549,
    1182911141497541140586615359230243592834259935951457923890100901143724702295750543180757886003762250822014770432406785975220712551,
    5452212151844264453115521770362712815300456788073870260637172006414987479914150831821202259862091373741738511579629040732909194883,
    6539010935836249262943170849578201335017375258186767902505297465587122376751466876714291620849745334794779375900778144335240097113,
    1966605170351883122720890324745439659237468374231783680395362721624862483865065649496199746596592488851227128040234894355126333577,
    4578487274208482739785211870551459143579948064875277467213620367693698827278583879146069992097475604136147868628836746016239588907]
{-
    Given (a,b,c) in Z/qZ and n, this function returns a scalar multiple of (a,b,c), (a',b',c'),
    such that (a')^2 + (b')^2 + (c')^2 = n mod q.
-}
proj :: (RandomGen t1) => t1 -> Integer -> Integer -> Integer -> Integer -> Integer -> Int -> [Integer]
proj g a b c n q effort = [a', b', c']
    where
        a' = euclid_mod (a*k) q
        b' = euclid_mod (b*k) q
        c' = euclid_mod (c*k) q
        u = fromJust $ inv_mod  q (a^2 + b^2 + c^2) -- inverse of (a^2 + b^2 + c^2) mod q
        k = fromJust $ run_bounded effort $ root_mod g q (u*n) -- square root of u*n mod q

{------------------------------------------------------------------------
wp and eta
------------------------------------------------------------------------}

{-
    This function picks lattices with random specifications, and returns the pair (eta, wp)
    for each prime in 'primes'.
-}
lift_bounded :: (RandomGen t1) => t1 -> Integer -> Int -> [(Double, Double)]
lift_bounded gen p effort = exp primes gen where
    exp (q:qs) g = (e, exponent):(exp qs $ snd $ split g) where

        -- choose a, b, c generically
        rand_input = take 3 $ randomRs (0, q) g
        [a, b, c] = generate_abc rand_input g
        generate_abc input k
            |qresidue (normm (Vector input)) q == 1 = input
            |otherwise = generate_abc (take 3 $ randomRs (0, q) $ snd $ split k) $ snd $ split k
        
        -- find minimal lift
        h = fromIntegral $ snd $ fromJust $ lift g a b c p q effort
        exponent = (3/8) * h * (log (fromIntegral p))/(log (fromIntegral q))
        
        -- compute eta, based on LLL-reduced basis
        [u1, u2, u3] = reduce (lattice a b c q) 0.75
        m = maximum $ map normm [u1, u2, u3]
        e = 0.5*(log $ fromIntegral m)/(log $ fromIntegral q)

    exp [] _ = []

{-
    lift_bounded with a = 1 fixed
-}
lift_bounded_one_fixed :: (RandomGen t1) => t1 -> Integer -> Int -> [(Double, Double)]
lift_bounded_one_fixed gen p effort = exp primes gen where
    exp (q:qs) g = (e, exponent):(exp qs $ snd $ split g) where
        [r2, r3] = take 2 $ randomRs (60 :: Int, 125 :: Int) g -- obtain 2 random integers
        
        -- set random bounds based on random numbers
        b2 = 0.1^r2
        b3 = 0.1^r3

        -- choose b, c based on the random bounds; will be randomly distributed on a log scale
        a = 1
        (g1, g2) = split g
        blist = take 1 $ randomRs (0, floor $ b2 * fromIntegral q) g1
        clist = take 1 $ randomRs (0, floor $ b3 * fromIntegral q) g2
        [b, c] = generate_abc (blist ++ clist) g
        generate_abc input k
            |qresidue (normm $ Vector ([a] ++ input)) q == 1 = input
            |otherwise = generate_abc [b', c'] (snd $ split k) where
                [b'] = take 1 $ randomRs (0, floor $ b2 * fromIntegral q) (fst $ split k)
                [c'] = take 1 $ randomRs (0, floor $ b3 * fromIntegral q) (snd $ split k)
        
        -- find minimal lift
        h = fromIntegral $ snd $ fromJust $ lift g a b c p q effort
        exponent = (3/8) * h * (log (fromIntegral p))/(log (fromIntegral q))
        
        -- compute eta, based on LLL-reduced basis
        [u1, u2, u3] = reduce (lattice a b c q) 0.75
        m = maximum $ map normm [u1, u2, u3]
        e = 0.5*(log $ fromIntegral m)/(log $ fromIntegral q)

    exp [] _ = []

{-
    lift_bounded with a = b = 1 fixed
-}
lift_bounded_two_fixed :: (RandomGen t1) => t1 -> Integer -> Int -> [(Double, Double)]
lift_bounded_two_fixed gen p effort = exp primes gen where
    exp (q:qs) g = (e, exponent):(exp qs $ fst $ split g) where
        [r3] = take 1 $ randomRs (60 :: Int, 125 :: Int) g -- obtain a random integer
        
        -- set random bounds based on random numbers
        b3 = 0.1^r3

        -- c based on the random bound; will be randomly distributed on a log scale
        a = 1
        b = 1
        clist = take 1 $ randomRs (0, floor $ b3 * fromIntegral q) g
        [c] = generate_abc clist g
        generate_abc [x] k
            |qresidue (normm (Vector [a,b,x])) q == 1 = [x]
            |otherwise = generate_abc [c'] (snd $ split k) where
                [c'] = take 1 $ randomRs (0, floor $ b3 * fromIntegral q) (snd $ split k)        
        -- find minimal lift
        h = fromIntegral $ snd $ fromJust $ lift g a b c p q effort
        exponent = (3/8) * h * (log (fromIntegral p))/(log (fromIntegral q))
        
        -- compute eta, based on LLL-reduced basis
        [u1, u2, u3] = reduce (lattice a b c q) 0.75
        m = maximum $ map normm [u1, u2, u3]
        e = 0.5*(log $ fromIntegral m)/(log $ fromIntegral q)

    exp [] _ = []

{-
    This function picks random small lattices, and returns the pair (eta, e)
    for each prime in 'primes'.
-}
lift_bounded_all_small :: (RandomGen t1) => t1 -> Integer -> Int -> [(Double, Double)]
lift_bounded_all_small gen p effort = exp primes gen where
    exp (q:qs) g = (e, exponent):(exp qs $ snd $ split g) where
        bound = 0.1^125

        -- choose a, b, c based on the small bound
        [ai, bi, ci] = take 3 $ randomRs (0, floor $ bound * fromIntegral q) g
        [a, b, c] = generate_abc ([ai, bi, ci]) g
        generate_abc [x, y, z] k
            |qresidue (normm (Vector [x, y, z])) q == 1 = [x, y, z]
            |otherwise = generate_abc [a', b', c'] (snd $ split k) where
                [a', b', c'] = take 3 $ randomRs (0, floor $ bound * fromIntegral q) (snd $ split k)
        
        -- find minimal lift
        h = fromIntegral $ snd $ fromJust $ lift g a b c p q effort
        exponent = (3/8) * h * (log (fromIntegral p))/(log (fromIntegral q))
        
        -- compute eta, based on LLL-reduced basis
        [u1, u2, u3] = reduce (lattice a b c q) 0.75
        m = maximum $ map normm [u1, u2, u3]
        e = 0.5*(log $ fromIntegral m)/(log $ fromIntegral q)

    exp [] _ = []

lift_bounded_random :: (RandomGen t1) => t1 -> Integer -> Int -> [(Double, Double)]
lift_bounded_random gen p effort = exp primes gen where
    exp (q:qs) g = (e, exponent):(exp qs $ snd $ split g) where
        [r1, r2, r3] = take 3 $ randomRs (60 :: Int, 125 :: Int) g -- obtain 3 random integers
        
        -- set random bounds based on random numbers
        b1 = 0.1^r1
        b2 = 0.1^r2
        b3 = 0.1^r3

        -- choose a, b, c based on the random bounds; will be randomly distributed on a log scale
        (g1, g2) = split g
        g3 = snd $ split g2
        alist = take 1 $ randomRs (0, floor $ b1 * fromIntegral q) g1
        blist = take 1 $ randomRs (0, floor $ b2 * fromIntegral q) g2
        clist = take 1 $ randomRs (0, floor $ b3 * fromIntegral q) g3
        [a, b, c] = generate_abc (alist ++ blist ++ clist) g
        generate_abc input k
            |qresidue (normm $ Vector input) q == 1 = input
            |otherwise = generate_abc [a', b', c'] (snd $ split k) where
                [a'] = take 1 $ randomRs (0, floor $ b1 * fromIntegral q) k1
                [b'] = take 1 $ randomRs (0, floor $ b2 * fromIntegral q) k2
                [c'] = take 1 $ randomRs (0, floor $ b3 * fromIntegral q) k3
                (k1, k2) = split k
                k3 = snd $ split k2
        
        -- find minimal lift
        h = fromIntegral $ snd $ fromJust $ lift g a b c p q effort
        exponent = (3/8) * h * (log (fromIntegral p))/(log (fromIntegral q))
        
        -- compute eta, based on LLL-reduced basis
        [u1, u2, u3] = reduce (lattice a b c q) 0.75
        m = maximum $ map normm [u1, u2, u3]
        e = 0.5*(log $ fromIntegral m)/(log $ fromIntegral q)

    exp [] _ = []