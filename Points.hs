{-
    This file generats a list of points of increasing distance from the origin.
-}

module Points where
import Vector

{------------------------------------------------------------------------
Interleaving List
------------------------------------------------------------------------}

{-
    Given a positive integer k, returns the list [0,1,-1,2,-2...k,-k]
-}
interleaved :: Integer -> [Integer]
interleaved k = concat $ [0] : (zipWith (\x y -> [x]++[y]) [1..(k)] [-1,-2..(-k)])

{------------------------------------------------------------------------
Main Function
------------------------------------------------------------------------}

{-
    Returns a list of all points in the rectangular prism |x_i| <= t_i from the center outwards.
    That is: each point in a rectangular prism centered at the origin contained in another is
    completely listed before the other points of the larger prism.
-}
points :: Integer -> Integer ->  Integer -> [Vector [Integer]]
points t1 t2 t3 = map (\x -> Vector x) (concat list)
    where
        m = maximum [t1,t2,t3]
        list = map (\k -> cube k t1 t2 t3) [0..m]

{------------------------------------------------------------------------
Split into listing each face of each prism.
------------------------------------------------------------------------}

{-
    Lists all points on the surface of the corresponding prism.
-}
cube :: Integer -> Integer -> Integer -> Integer -> [[Integer]]
cube 0 _ _ _ = [[0,0,0]]
cube _ 0 0 0 = [[0,0,0]]
cube k t1 t2 t3 =
    if (a == b && b == c) then list1 ++ list2 ++ list3' ++ list4' ++ list5'' ++ list6''
    else if (a == b && b > c) then list1 ++ list2 ++ list3' ++ list4'
    else if (a == c && c > b) then list1 ++ list2 ++ list5' ++ list6'
    else if (b == c && c > a) then list3 ++ list4 ++ list5b ++ list6b
    else if (a > max b c) then list1 ++ list2
    else if (b > max a c) then list3 ++ list4
    else if (c > max a b) then list5 ++ list6
    else []
     
    where
        a = min k t1
        b = min k t2
        c = min k t3

        list1 = map (extendx a) (spiral b c)
        list2 = map (extendx (-a)) (spiral b c)

        list3 = map (extendy b) (spiral a c)
        list3' = map (extendy b) (spiral (a - 1) c)
        list4 = map (extendy (-b)) (spiral a c)
        list4' = map (extendy (-b)) (spiral (a - 1) c)

        list5 = map (extendz c) (spiral a b)
        list5' = map (extendz c) (spiral (a - 1) b)
        list5b = map (extendz c) (spiral a (b - 1))
        list5'' = map (extendz c) (spiral (a - 1) (b - 1))
        list6 = map (extendz (-c)) (spiral a b)
        list6' = map (extendz (-c)) (spiral (a - 1) b)
        list6b = map (extendz (-c)) (spiral a (b - 1))
        list6'' = map (extendz (-c)) (spiral (a - 1) (b - 1))

extendx :: Integer->[Integer]->[Integer]
extendx x [a,b]= [x,a,b]

extendy :: Integer->[Integer]->[Integer]
extendy x [a,b]= [a,x,b]

extendz :: Integer->[Integer]->[Integer]
extendz x [a,b]= [a,b,x]

{------------------------------------------------------------------------
List points on each face
------------------------------------------------------------------------}

{-
    Returns all integral points within the rectangle centered at the origin with lengths 2a and 2b
-}
spiral :: Integer -> Integer -> [[Integer]]
spiral a b = concat $ map (\k -> square (min k a) (min k b)) [0..max a b]

{-
    Lists all points on the boundary of a single square.
-}
square :: Integer -> Integer -> [[Integer]]
square 0 0 = [[0,0]]
square a b = if (a == b) then list1' ++ list2' ++ list3' ++ list4' else list1 ++ list2
    where
        list1 = if (a > b) then appendx a [-b..b] else appendy b [-a..a]
        list2 = if (a > b) then appendx (-a) [-b..b] else appendy (-b) [-a..a]
        list1' = appendx a [-a..a]
        list2' = appendx (-a) [-a..a]
        list3' = appendy a [(-a + 1)..(a - 1)]
        list4' = appendy (-a) [(-a + 1)..(a - 1)]

appendx :: Integer -> [Integer] -> [[Integer]]
appendx a (x:xs) = [a,x] : (appendx a xs)
appendx _ [] = []

appendy :: Integer -> [Integer] -> [[Integer]]
appendy a (x:xs) = [x,a] : (appendy a xs)
appendy _ [] = []